PREFIX=
CC=cc
CFLAGS=-O3 -std=gnu99
LIBS=-lncurses -lm

all: nuke

nuke: *.h *.c
	$(CC) nuke.c $(LIBS) $(CFLAGS) -o nuke

clean:
	rm -f nuke

install: nuke
	@prefix="$(PREFIX)"; \
	if [[ ! $$prefix ]]; then \
		read -p $$'\033[1mWhere do you want to install? (default: /usr/local) \033[0m' prefix; \
	fi; \
	if [[ ! $$prefix ]]; then \
		prefix="/usr/local"; \
	fi; \
	mkdir -pv $$prefix/bin $$prefix/share/man/man1 \
	&& cp -v nuke $$prefix/bin/ \
	&& cp -v doc/nuke.1 $$prefix/share/man/man1/

uninstall:
	@prefix="$(PREFIX)"; \
	if [[ ! $$prefix ]]; then \
		read -p $$'\033[1mWhere do you want to uninstall from? (default: /usr/local) \033[0m' prefix; \
	fi; \
	if [[ ! $$prefix ]]; then \
		prefix="/usr/local"; \
	fi; \
	echo "Deleting..."; \
	rm -rvf $$prefix/bin/nuke $$prefix/share/man/man1/nuke.1

