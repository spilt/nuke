#ifndef __colors
#define __colors
const int WHITE = 1, YELLOW = 2, RED = 3, BLUE = 4, GREEN = 5, BLACK = 6, WHITE_BG = 7,
    BLACK_ON_RED = 8, BLACK_ON_GREEN = 9;
#endif
