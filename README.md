# nuke

*nuke* is a program that does a simple task: it nukes files from orbit. Because
sometimes `rm -rf` is just too good for a file.

![Preview](https://bitbucket.org/spilt/nuke/downloads/nuke.gif)

## Installation

The only requirement is [ncurses](https://www.gnu.org/software/ncurses/), which probably comes with your platform. Use `make` to build, `sudo make install` to install.

## Usage

`nuke file1 file2...`
